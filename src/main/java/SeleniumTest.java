import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by RossiM on 04.02.2016.
 */
public class SeleniumTest {

	public static String PHANTOM = "phantom";
	public static String CHROME = "chrome";

	private String browser;
	private String token;

	private WebDriver _driver;
	private WebDriverWait _wait;
	private Date datetime = new Date();

	public static JavascriptExecutor _js;

	private String DRIVER_EXECUTABLE;

	Long timestamp = new Date().getTime();

	public SeleniumTest(String browser, String token) {
		this.browser = browser;
		this.token = token;
	}

	public void setUp() {
		Capabilities caps = new DesiredCapabilities();
		((DesiredCapabilities) caps).setJavascriptEnabled(true);
		((DesiredCapabilities) caps).setCapability("takesScreenshot", true);

		InputStream inStream = null;
		OutputStream outStream = null;

		if (browser.equals(PHANTOM)) {
			DRIVER_EXECUTABLE = System.getProperty("user.dir") + "\\phantomjs.exe";
			inStream = getClass().getResourceAsStream("phantomjs.exe");
			((DesiredCapabilities) caps).setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, DRIVER_EXECUTABLE);
		} else {
			DRIVER_EXECUTABLE = System.getProperty("user.dir") + "\\chromedriver.exe";
			inStream = getClass().getResourceAsStream("chromedriver.exe");
			((DesiredCapabilities) caps).setCapability(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, DRIVER_EXECUTABLE);
		}

		try {
			File targetFile = new File(DRIVER_EXECUTABLE);
			outStream = new FileOutputStream(targetFile);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, length);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (browser.equals(PHANTOM)) {
			_driver = new PhantomJSDriver(caps);
		} else {
			_driver = new ChromeDriver(caps);
		}

		_js = (JavascriptExecutor) _driver;
		_wait = new WebDriverWait(_driver, 10);

		_driver.manage().window().setSize(new Dimension(1920, 1080));
	}

	public void test() {
		// load loginpage
		_driver.get("https://haz-daily.haufe.de/haz/index.html");

		// create authentication cookie
		_js.executeScript(
				"jQuery.cookie('SaasEcoUser'," + token + ", {path: '/haz-rest/', secure: true, raw: true}); jQuery.cookie('secToken', null, {path: '/haz-rest/', secure: true});");

		takeTimestamp();

		///// startpage ///////

		// reload login page
		_driver.get("https://haz-daily.haufe.de/haz/index.html");

		_wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver webDriver) {
				boolean retVal = _driver.findElement(By.xpath("//li[@data-haz-role='OPT_WRITE']")) != null;
				return retVal;
			}
		});

		long delta = getDelta();
		sendStatistic(delta, "startpage_loaded");
		System.out.println("Duration is: " + delta + " ms for startpage loading");

		takeScreenshot("startpage_loaded");

		/////// settingspage //////////

		takeTimestamp();

		_js.executeScript("arguments[0].click();", _driver.findElement(By.xpath("//li[@data-haz-role='OPT_WRITE']")));

		_wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver webDriver) {
				return _driver.findElement(By.xpath("//input[@value='Speichern']")) != null;
			}
		});

		delta = getDelta();
		sendStatistic(delta, "settingspage_loaded");
		System.out.println("Duration is: " + delta + " ms for settingspage loading");
		takeScreenshot("settingspage_loaded");

		/////////// company settings page ///////////////////

		takeTimestamp();

		_js.executeScript("arguments[0].click();", _driver.findElement(By.cssSelector("#tab-header-settings-company span")));

		_wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver webDriver) {
				return _driver.findElement(By.xpath("//input[@id='profileName']")) != null;
			}
		});

		delta = getDelta();
		sendStatistic(delta, "companysettingspage_loaded");
		System.out.println("Duration is: " + delta + " ms for companysettingspage loading");
		takeScreenshot("companysettingspage_loaded");
	}

	public void cleanUp() {
		_driver.quit();
	}

	private void takeScreenshot(String name) {
		String folder = "./screenshots/";
		String filename = name + ".png";
		File scrFile = ((TakesScreenshot) _driver).getScreenshotAs(OutputType.FILE);
		try {
			File f = new File(folder);
			f.mkdir();

			f = new File(folder + filename);
			f.delete();
			FileUtils.copyFile(scrFile, f);
			scrFile.delete();
			System.out.println("Screenshot taken: " + name);
		} catch (Exception e) {
			System.out.println("Could not take screenshot: " + filename);
		}
	}

	private void takeTimestamp() {
		this.timestamp = new Date().getTime();
	}

	private Long getDelta() {
		try {
			long delta = new Date().getTime() - this.timestamp;
			this.timestamp = null;
			return delta;
		} catch (Exception e) {
			System.out.println("No timestamp for calculation set! First call takeTimestamp()");
		}

		return null;
	}

	private void sendStatistic(long duration, String action) {
		try {
			URL url = new URL("http://localhost:9200/performance-haz/test");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");

			JSONObject json = new JSONObject();
			json.append("duration", duration);
			json.append("action", action);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			json.append("datetime", format.format(this.datetime));

			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();

			StringBuilder sb = new StringBuilder();
			int HttpResult = conn.getResponseCode();
			if (HttpResult == HttpURLConnection.HTTP_OK || HttpResult == HttpURLConnection.HTTP_CREATED) {
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}

				br.close();

				System.out.println("" + sb.toString());
			} else {
				System.out.println(conn.getResponseMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
