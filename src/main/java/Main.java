/**
 * Created by RossiM on 04.02.2016.
 */
public class Main {

	public static void main(String[] args) throws Exception {
		String browser = null;
		String token = null;

		if (args.length != 2) {
			System.out.println("Need 2 arguments: \n1. Browser ('phantom'/'chrome')\n2. Valid security token");
			return;
		}

		if (args[0].equals(SeleniumTest.CHROME)) {
			browser = SeleniumTest.CHROME;
		} else {
			browser = SeleniumTest.PHANTOM;
		}

		token = args[1];

		System.out.println("Browser: " + browser);

		SeleniumTest seleniumTest = new SeleniumTest(browser, token);

		seleniumTest.setUp();
		seleniumTest.test();
		seleniumTest.cleanUp();
	}
}
